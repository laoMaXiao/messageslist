Vue.component('vInput',{
     props:{
         value:{
             type:[String,Number],
             default:''
         }
     },
     data:function(){//父组件修改的值value可以通过watch监听传递，子组件修改值myvalue也可以在watch中告知父组件
         return {
             myvalue:this.value
         }
     },
     watch:{
        value:function(val){
            this.myvalue=val;
        },
        myvalue:function(){
            this.$emit('input',this.myvalue)
        }
     },
     template:'<div>\
     <span>昵称：</span>\
     <input type="text" v-model="myvalue">\
     </div>',
    
});

Vue.component('vTextarea',{
    props:{
        value:{
            type:String,
            default:''
        }
    },
    data:function(){
       return {
           myvalue:this.value
       }
    },
    watch:{
        myvalue:function(){
            this.$emit('input',this.myvalue)
        },
        value:function(val){
            this.myvalue=val
        }
    },
    template:'<div>\
    <span>留言内容：</span>\
    <textarea placeholder="请输入....." type="text" v-model="myvalue" ref="message"></textarea>\
    </div>',
    methods:{
      focus:function(){
          this.$refs.message.focus();
      }
    }
})