Vue.component('messagelist',{
    props:{
        list:{
            type:Array,
            default:function(){
                return []
            }
        }
    },
    template:'<div>\
    <div class="list-item"  v-for="(list,index) in list">\
    <span>{{list.name}} :</span>\
    <div class="list-msg">\
    <p >{{list.message}}</p>\
    <a class="list-reply" v-on:click="handleReply(index)">回复</a>\
    </div>\
    </div>\
    </div>',
    methods:{
        handleReply:function(index){
            this.$emit('reply',index);
        }
    }
})